from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, View
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator


@method_decorator(ensure_csrf_cookie, name='dispatch')
class IndexView(TemplateView):
    template_name = 'index.html'


class LogoutView(View):
    def post(self, request):
        logout(request)
        return HttpResponseRedirect('/')
