from celery import Celery
import os
import requests

from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'steam_trade.settings')


app = Celery('sttrade')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
