from django.utils import timezone
from django.conf import settings
from django.core.cache import cache
from django.utils.deprecation import MiddlewareMixin


class UserOnlineMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if request.user.is_authenticated():
            cache.set('%s:online' % (request.user.pk),
                      timezone.now(),
                      settings.USER_ONLINE_TIMEOUT)
