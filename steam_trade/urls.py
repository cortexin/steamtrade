"""steam_trade URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


from notifications.views import NotificationListView
from .views import IndexView, LogoutView


urlpatterns = []

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^logout$', LogoutView.as_view(), name='logout'),
    url('', include('social_django.urls', namespace='social')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/notifications$', NotificationListView.as_view(),
        name='notification-list'),
    url(r'^api/', include('trading.urls')),
    url(r'^api/', include('authentication.urls')),
    url(r'^.*', IndexView.as_view(), name='index'),

    # url(r'^fail-payment/$', TemplateView.as_view(template_name='fail.html'), name='payment_fail'),
    # url(r'^success-payment/$', TemplateView.as_view(template_name='success.html'), name='payment_success'),
    # url(r'^yandex-money/', include('yandex_money.urls')),
]
