from django.conf import settings
from django.utils import timezone

from steam_trade.celery import app
from trading.channels.notifications import offer_ready
from trading.models import Offer

from .exceptions import NoItemsReceived
from .utils import to_gameoptions

from datetime import timedelta, datetime
import operator


@app.task(bind = True)
def validate_offer(self, seller, offer_pk, min_time):
    db_offer = Offer.objects.get(pk=offer_pk)
    bot = db_offer.bot
    print('Im here!')

    # convert to 32bit
    seller = int(seller) - 76561197960265728

    # offer should not have been sent before that time
    min_time = datetime.now() - timedelta(seconds=min_time + 20 + \
                                          self.request.retries * 15)

    pending_offers = bot.client.get_trade_offers()
    pending_offers = pending_offers['response']
    pending_offers = pending_offers['trade_offers_received']

    pending_offers = list(filter(lambda x: \
        datetime.fromtimestamp(x['time_created']) >= min_time and \
        x['accountid_other'] == seller and \
        len(x['items_to_receive']) == 1,
    pending_offers))

    if pending_offers:
        # there are matching items in our inventory from the provided user
        pending_offers.sort(key=operator.itemgetter('time_created'))

        item = list(pending_offers[0]['items_to_receive'].values())[0]

        if item_matches_offer(item, db_offer):
            # these items are of required type and amount
            db_offer.trade_id = pending_offers[0]['tradeofferid']
            db_offer.save()

            if post_offer:
                # the user is waiting to receive the item after validation
                offer_ready(offer.seller, offer)
            return True
    else:
        print("there is a problem with validation")
        raise self.retry(exc=NoItemsReceived)

    db_offer.delete()
    return False

@app.task(bind = True, default_retry_delay=settings.OFFER_VALIDATION_DELAY['PRE_OFFER'])
def check_item_in_inventory(self, pk):
    """
    Check whether the bot has given items in given q.
    W: Possible race condition between 2 purchases
    """
    offer = Offer.objects.get(pk=pk)
    bot = offer.bot

    inv = bot.client.get_my_inventory(to_gameoptions(offer.item.appid))

    if has_item(inv, offer.pk, offer.amount):
        offer_ready(offer.seller, offer)
        return True
    else:
        print("there is a problem with validation")
        raise self.retry(exc=NoItemsReceived)


def validate_preoffer(seller, offer_pk):
    validate_offer.apply_async((seller.social_auth.last().uid,
                                offer_pk,
                                settings.OFFER_VALIDATION_DELAY['PRE_OFFER']),
                                countdown=30,
                                retry=False)


def validate_postoffer(offer_pk):
    cd = settings.OFFER_VALIDATION_DELAY['POST_OFFER']
    check_item_in_inventory.apply_async((offer_pk, ),
                                 countdown=cd)


def has_item(inventory, item, amount):
    items = filter(lambda x: x[1]['market_hash_name'] == item,
                   inventory.items())

    try:
        list(items)[:amount]
        return True
    except IndexError:
        return False
