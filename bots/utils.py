from steampy.client import Asset
from steampy.utils import GameOptions


def to_gameoptions(id):
    return {
        '440': GameOptions.TF2,
        '570': GameOptions.DOTA2,
        '730': GameOptions.CS
    }[id]


def extract_assets(inventory: dict, amount: int, name: str, game: GameOptions):
    items = filter(lambda x: x[1]['market_hash_name'] == name,
                   inventory.items())

    try:
        items = list(items)[:amount]
    except IndexError:
        # there are less items in the inventory than is specified in the offer
        # should not happen, no idea what to do in this case
        pass

    return [Asset(item[0], game) for item in items]


def item_matches_offer(item, offer):
    return (int(item['amount']), item['market_hash_name']) == (db_offer.amount, db_offer.item.pk)
