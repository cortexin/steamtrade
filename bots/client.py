from steampy.client import SteamClient
from steampy.login import LoginExecutor
from django.conf import settings


class DBSteamClient(SteamClient):
    def __init__(self, bot) -> None:
        super().__init__(bot.apikey)
        self.steam_guard = dict(steamid=bot.steamid,
                                shared_secret=bot.shared_secret,
                                identity_secret=bot.identity_secret)
        self.username = bot.username
        self.password = bot.password

    def login(self) -> None:
        LoginExecutor(self.username, self.password,
                      self.steam_guard['shared_secret'], self._session).login()
        self.was_login_executed = True
