#monkey patch

from steampy import guard

_gdid = guard.generate_device_id

guard.generate_device_id = lambda sid: _gdid(str(sid))
