from django.db import models
from django.conf import settings
from steampy.client import Asset
import dill

from queue import Queue
from typing import Tuple

from .client import DBSteamClient
from .utils import to_gameoptions, extract_assets

r = settings.REDIS_CONN(False)


class TradeBot:
    def __init__(self, username, password, steamid, shared_secret,
                 identity_secret, url, apikey):
        self.username = username
        self.password = password
        self.steamid = steamid
        self.shared_secret = shared_secret
        self.identity_secret = identity_secret
        self.apikey = apikey
        self.url = url
        self.counter = 0

        self.login()


    @property
    def client(self):
        if not hasattr(self, '_client'):
            setattr(self, '_client', DBSteamClient(self))
        return self._client

    def login(self):
        self.client.login()

    def update(self):
        r.hset('tradebots', self.username, dill.dumps(self))
        print('%s Updated' % self.username)

    def accept(self, offer):
        # todo: send notification to accept
        print(offer.trade_id)
        self.client.accept_trade_offer(offer.trade_id)

    def make_offer(self, offer, request=True):
        game = to_gameoptions(offer.item.appid)
        print(offer)

        my_inv = []
        their_inv = []

        if request:
            seller = offer.seller.social_auth.first().uid
            their_inv = self.client.get_partner_inventory(seller, game)
            their_inv = extract_assets(their_inv, offer.amount,
                                       offer.item.market_hash_name,
                                       game)
        else: #transfer
            my_inv = self.client.get_my_inventory(game)
            my_inv = extract_assets(my_inv, offer.amount,
                                    offer.item.market_hash_name,
                                    game)

        message = 'Покупка в steam trade. Код %s' % offer.code

        print(message, offer.seller)
        print(my_inv, their_inv)

        r = self.client.make_offer_with_url(items_from_me=my_inv,
                                            items_from_them=their_inv,
                                            trade_offer_url=offer.buyer.trade_offer_url,
                                            message=message)
        if request:
            offer.trade_id = r['tradeofferid']
            offer.save()

    def transfer(self, offer):
        self.make_offer(offer, request=False)

    def request(self, offer):
        self.make_offer(offer, request=True)


class BotManager:
    def __init__(self):
        self.all = {}
        self.free = Queue()
        self.busy = set()
        self.boot()

    def boot(self):
        bot_pickles = r.hgetall('tradebots')
        bots = {name.decode(): dill.loads(pickle) for
                name, pickle in bot_pickles.items()}
        self.all = bots
        for bot in bots.values():
            if not bot.client.is_session_alive() and bot.username == 'fantomtradebot2':
                try:
                    bot.login()
                    bot.update()
                except:
                    print('Failed to log in')
            self.free.put(bot)

    def add(self, **kwargs):
        bot = TradeBot(**kwargs)
        r.hset('tradebots', bot.username, dill.dumps(bot))
        self.all[bot.username] = bot
        self.free.put(bot)

    def get_free(self):
        # bot = self.free.get()
        # self.free.put(bot)
        # return bot
        return self.all['fantomtradebot2']

    def return_free(self, name):
        pass

    def is_free(self, name: str) -> bool:
        return name in self.free.queue

    def is_busy(self, name: str) -> bool:
        return self.busy.issuperset([name])

    def get_by_name(self, name) -> Tuple[object, bool]:
        bot = self.all.get(name, None)
        if not bot:
            pass
        return bot

Manager = BotManager()
