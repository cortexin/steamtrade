(function () {
  angular.module('steam', [
    'ngMaterial',
    'ngResource',
    'ngRoute',
    'ngWebSocket',
    'angularUtils.directives.dirPagination',
    'checklist-model'])
})()
