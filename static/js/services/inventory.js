(function () {
  'use strict'

  angular.module('steam').factory('Inventory', function ($http, $cacheFactory) {
    let cache = $cacheFactory('inventory-cache')

    let Inventory = {
      fetch: fetch
    }

    return Inventory

    /*
    / Fetch clients in-game inventory for app appId.
    */
    function fetch (appId, username, ignoreCached = false) {
      if (!ignoreCached) {
        let cached = cache.get(appId + '-' + username)
        if (cached) // there is a cached copy
          return cached
      }
      return $http.post('/api/fetch', {app_id: appId}).then((r) => {
        cache.put(appId + '-' + username, r.data)
        return r
      })
    }
  })
})()
