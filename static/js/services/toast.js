(function () {
  'use strict'

  const TOAST_URL = '/static/templates/toasts/'

  angular.module('steam').factory('Toast', function ($mdToast, $http) {
    let Toast = {}

    Toast.notify = function (locals) {
      const toast = {
        templateUrl: TOAST_URL + 'notify.html',
        locals: locals
      }
      return $mdToast.show(toast)
    }

    Toast.confirm = function (locals, conf, decl) {
      const toast = {
        templateUrl: TOAST_URL + 'confirm.html',
        controller: function ($http, locals) {
          let vm = this
          vm.locals = locals
          vm.confirm = $mdToast.hide
          vm.decline = $mdToast.cancel
        },
        controllerAs: 'vm',
        locals: locals
      }

      return $mdToast.show(toast).then(function () {
        $http.post(conf)
      }, function () {
        $http.post(decl)
      })
    }

    return Toast
  })
})
