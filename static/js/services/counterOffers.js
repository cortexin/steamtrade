(function () {
  'use strict'
  angular.module('steam').factory('CounterOffers', function ($http) {
    const URL = '/api/counteroffers/'
    let CounterOffers = {
      accept: _accept,
      create: _create,
      list: _list,
      update: _update,
      delete: _delete
    }

    return CounterOffers

    function _accept (cOffer) {
      return $http.post(URL + cOffer.id + '/accept/')
    }

    function _create (offer, price) {
      return $http.post(URL, {
        price: price,
        offer: offer.id
      })
    }

    function _list (offer) {
      return $http.get(URL + 'list/' + offer.id + '/')
    }

    function _update (cOffer, price) {
      return $http.patch(URL + cOffer.id + '/', {price: price})
    }

    function _delete (cOffer) {
      return $http.delete(URL + cOffer.id + '/')
    }
  })
})()
