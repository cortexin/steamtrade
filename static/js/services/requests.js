(function () {
  'use strict'

  const URL = '/api/requests/'

  angular.module('steam').factory('Requests', function ($http) {
    let Requests = {
      submit: _submit,
      list: _list,
      retrieve: _retrieve,
      update: _update,
      remove: _remove
    }

    return Requests

    function _submit (request) {
      return $http.post(URL, request)
    }

    function _list () {
      return $http.get(URL)
    }

    function _retrieve (item) {
      return $http.get(URL + item.classid + '/')
    }

    function _update (request) {
      return $http.patch(URL + request.item + '/', request)
    }

    function _remove (id) {
      return $http.delete(URL + id + '/')
    }
  })
})()
