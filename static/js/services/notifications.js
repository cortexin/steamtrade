'use strict'

angular.module('steam').factory('Notifications', function ($websocket, $http, $mdToast, $log) {
  let Notifications = {
    all: [], // all notification messages
    profileSettings: {},
    num: 0
  }

  const wsUrl = 'ws://' + window.location.host + '/connect'

  let socket = $websocket(wsUrl)
  $log.info('Connecting to ' + wsUrl)

  socket.onOpen(() => $log.info('Successfully connected'))

  socket.onMessage(function (msg) {
    let data = angular.fromJson(msg.data)
    $log.info('Got data: ', data)

    if (data.type != 'settings') {
      Notifications.all.unshift(data)
    }

    switch (data.type) {
      case 'notify':
        _showNotify(data)
        break
      case 'confirm':
        _showConfirm(data)
        break
      case 'update':
        _update(data)
        break
      case 'settings':
        Notifications.profileSettings = data.payload
        break
      case 'offer':
        console.log(data.payload)
        Notifications.offer = data.payload
        break
      default:
        $log.info('Unknown notification type')
    }
  })

  function _update ({new_notifications}) {
    Notifications.all = new_notifications
    Notifications.num = new_notifications.length
  }

  function _showNotify ({payload}) {
    Notifications.num++
    if (Notifications.profileSettings.allow_sound)
      new Audio('/media/notification.wav').play()
    if (Notifications.profileSettings.allow_toasts)
      $mdToast.show(SUCCESS_TOAST(payload))
  }

  function _showConfirm ({payload, confirm, decline}) {
    Notifications.num++
    if (Notifications.profileSettings.allow_sound)
      new Audio('/media/notification.wav').play()
    $mdToast.show({
      templateUrl: TOAST_URL + 'confirm.html',
      controller: 'ToastConfirmController',
      controllerAs: 'vm',
      locals: {message: payload},
      hideDelay: 0
    }).then(() => $http.post(confirm),
            () => $http.post(decline))
  }

  Notifications.flush = () => socket.send(JSON.stringify({action: 'flush'}))
  Notifications.send = (type, msg) => socket.send(JSON.stringify({
    action: type,
    content: msg
  }))

  return Notifications
})
