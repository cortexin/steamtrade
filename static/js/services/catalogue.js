(function () {
  'use strict'

  angular.module('steam').factory('Catalogue', function ($http) {
    const URL = '/api/items/'

    let Catalogue = {
      get: _listItems,
      offers: _itemDetails
    }

    return Catalogue

    function _listItems ({search}, appid, page, filters) {
      let params = {
        page: page
      }
      if (search)
        params.search = search
      if (filters.length)
        params.filters = filters

      return $http.post('/api/catalogue/' + appid, params)
    }

    function _itemDetails (classid) {
      classid = window.encodeURIComponent(classid)
      return $http.get(URL + classid + '/')
    }
  })
})()
