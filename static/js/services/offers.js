(function () {
  'use strict'
  angular.module('steam').factory('Offers', function ($http) {
    const URL = '/api/offers/'

    let Offers = {
      submit: _submit,
      list: _list,
      cancel: _cancel
    }

    return Offers


    function _submit (offer, {market_hash_name}) {
      offer.item = market_hash_name
      return $http.post(URL, offer)
    }

    function _list () {
      return $http.get(URL)
    }

    function _cancel (offer) {
      return $http.delete(URL + offer.id + '/')
    }
  })
})()
