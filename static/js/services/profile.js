(function () {
  'use strict'

  angular.module('steam').factory('Profile', function ($http) {
    let Profile = {
      get: _get,
      update: _update
    }

    return Profile

    function _get () {
      return $http.get('/api/profile/')
    }

    function _update (profile) {
      return $http.patch('/api/profile/', profile)
    }
  })
})()
