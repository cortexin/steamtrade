(function () {
  'use strict'
  angular.module('steam').controller('NavbarController', function ($scope, $http) {
    $scope.logout = () => $http.post('/logout').then(() => { window.location = '/' })
  })
})()
