(function () {
  'use strict'

  angular.module('steam').controller('ProfileController', function ($scope, $rootScope, Profile, Requests) {
    Profile.get().then(
      ({data}) => {
        $scope.profile = data
        $scope.uProfile = angular.copy($scope.profile)
      },
      ({data}) => $scope.error_toast(data.detail)
    )

    $scope.update = () => Profile.update($scope.uProfile).then(
      ({data}) => {
        $scope.profile = data
        $scope.success_toast('Настройки успешно обновлены')
        $rootScope.notifications.profileSettings.url_enabled = true
      },
      ({data}) => {
        $scope.error_toast(data.detail)
        $scope.cancel()
      }
    )

    $scope.removeRequest = (request, i) => Requests.remove(request.item).then(
      () => $scope.requests.splice(i, 1),
      ({data}) => $scope.error_toast(data.detail)
    )

    $scope.showRequests = function () {
      $scope.requestTab = true
      Requests.list().then(
        ({data}) => { $scope.requests = data },
        ({data}) => $scope.error_toast(data)
      )
    }

    $scope.hideRequests = () =>{ $scope.requestTab = false }

    $scope.cancel = () => { $scope.uProfile = angular.copy($scope.profile) }
  })
})()
