(function () {
  'use strict'

  let SELL_DIALOG = (e, item) => ({
    controller: 'SellDialogController',
    templateUrl: '/static/templates/dialogs/sell.html',
    targetEvent: e,
    locals: {
      item: item
    },
    clickOutsideToClose: true
  })
  function URL_DIALOG (url) {
    return {
      controller: 'BotUrlDialogController',
      templateUrl: '/static/templates/dialogs/bot.html',
      locals: {url}
    }
  }

  angular.module('steam').controller('InventoryController', function (Notifications, $scope, $window,
      $sce, $mdToast, $mdDialog, Inventory, Offers) {
    $scope.selectedItem = null
    $scope.trustAsHtml = $sce.trustAsHtml
    try { // try to retrieve username from the document
      $scope.username = document.getElementById('username').innerText
    } catch (e) { // client is not logged in
      $scope.username = undefined
    }
    $scope.games = [
      {
        name: 'Team Fortress 2',
        ID: 440,
        items: null,
        totalItems: 0
      },
      {
        name: 'Dota 2',
        ID: 570,
        items: null,
        totalItems: 0
      },
      {
        name: 'Counter-Strike: Global Offensive',
        ID: 730,
        items: null,
        totalItems: 0
      }
    ]
    $scope.selectedGame = $scope.games[0]

    $scope.sellItem = function (event, item) {
      if (!$scope.notifications.profileSettings.url_enabled) {
        $mdToast.show(ERROR_TOAST(URL_NOT_ENABLED))
        return
      }

      $mdDialog.show(SELL_DIALOG(event, item)).then((offer) => {
        Offers.submit(offer, item).then(({data}) => offerSubmitted(data, item))
      }).catch(
        ({detail}) => $mdToast.show(ERROR_TOAST(detail)))

      function offerSubmitted (data, item) {
        item.amount_offered += data.offer.amount
        $mdToast.show(SUCCESS_TOAST('Успешно добавлено'))

        // redirect to steam and schedule validation
        if (data.url) {
          $window.open(data.url, "_blank")
          $scope.notifications.send('validate_offer', {offer: data.offer.id})
          const alertText = offer.send_offer ? 'У Вас есть 15 минут чтобы отправить оффер боту. В противном случае предложение будет удалено из базы' : 'Вы получите оффер на ваше предложение когда на него найдетя покупатель'
          $mdDialog.alert()
            .title('Ваше предложение добавлено в базу.')
            .textContent(alertText)
            .ok('OK')
            .targetEvent(event)
        }
      }
    }

    $scope.gameSelected = (game) => {
      $scope.selectedGame = game
      if (!game.items && $scope.username)
        // fetch ingame items if they have not been fetched yet
        fetchItems(game)
      else
        $scope.selectedItem = game.items[0]
    }

    /*
    / Refresh in-game inventory directly from the server
    */
    $scope.refreshItems = (game) => fetchItems(game, true)

    function fetchItems (game, ignoreCached = false) {
      game.error = undefined

      let response = Inventory.fetch(game.ID, $scope.username, ignoreCached)
      if (response.$$state) {
        response.then(
          ({data}) => { game.items = data.items }
        ).catch(
          ({data}) => { game.error = data.detail }
        )
      } else {
        game.items = response.items
      }
    }
  })
})()
