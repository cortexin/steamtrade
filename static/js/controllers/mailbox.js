angular.module('steam').controller('MailboxController', function ($scope, $http) {
  $scope.notifications.flush()
  $scope.newNots = $scope.notifications.all
  $scope.oldNots = []
  $scope.links = {}

  $scope.fetch = (url) => { // fetch old notifications
    $http.get(url).then(
      ({data}) => {
        $scope.oldNots = data.results
        $scope.links.next = data.next
        $scope.links.prev = data.previous
      },
      ({data}) => $scope.error_toast(data)
    )
  }

  $scope.fetch('/api/notifications')

  $scope.confirm = (ntfn, i) => _reply(ntfn, ntfn.confirm, true, i)
  $scope.decline = (ntfn, i) => _reply(ntfn, ntfn.decline, false, i)

  function _reply (ntfn, url, result, i) {
    console.log(ntfn)
    $http.post(url).then(
      () => {
        ntfn.result = result
        $scope.oldNots.unshift(ntfn)

        $scope.newNots.splice(i, 1)
      },
      ({data}) => $scope.error_toast(data)
    )
  }
})
