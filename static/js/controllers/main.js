(function () {
  'use strict'
  angular.module('steam').controller('MainController', function ($scope, $location) {
    $scope.url = (url) => $location.url(url)
  })
})()
