'use strict'
angular.module('steam').controller('CounterOfferDialogController', function($mdDialog, CounterOffers, offer, update) {
  let vm = this
  if (update) {
    vm.update = true
    vm.cOffer = offer.counter_offers[0]
    vm.price = vm.cOffer.price
  }

  vm.min = Math.floor(offer.price * 0.6)
  vm.max = offer.price

  vm.submit = () => $mdDialog.hide(vm.price)

  vm.cancel = $mdDialog.cancel

  vm.delete = () =>CounterOffers.delete(vm.cOffer).then(
    () => $mdDialog.cancel(true),
    ({data}) => vm.error_toast(data)
  )
})
