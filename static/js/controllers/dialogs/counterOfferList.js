'use strict'

angular.module('steam').controller('CounterOfferListController', function ($mdDialog, CounterOffers, offer) {
  let vm = this

  CounterOffers.list(offer).then(
    ({data}) => { vm.counterOffers = data },
    ({data}) => vm.error_toast(data)
  )

  vm.accept = (counterOffer) => CounterOffers.accept(counterOffer).then(
    () => $mdDialog.hide(),
    ({data}) => vm.error_toast(data)
  )
})
