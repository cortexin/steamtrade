(function () {
  'use strict'

  angular.module('steam').controller('DemandOfferDialogController', function ($scope, $mdDialog, Requests, item, update) {
    $scope.item = item
    if (update)
      Requests.retrieve(item).then(function ({data}) {
        $scope.price = data.price
        $scope.amount = data.amount
      })

    $scope.submit = function () {
      $mdDialog.hide({
        price: $scope.price,
        amount: $scope.amount,
        item: $scope.item.classid
      })
    }

    $scope.cancel = $mdDialog.cancel
  })
})()
