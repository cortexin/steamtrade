(function () {
  'use strict'

  angular.module('steam').controller('SellDialogController', function ($scope, $http, $mdDialog, item) {
    $scope.item = item

    $scope.totalAmount = $scope.item.amount
    $scope.minAmount = 1
    $scope.send_offer = false

    $scope.submit = () => $mdDialog.hide({
        price: $scope.price,
        amount: $scope.minAmount,
        allow_counteroffers: $scope.allow_counteroffers,
        appid: item.appid,
        classid: item.classid,
        instanceid: item.instanceid,
        send_offer: $scope.send_offer,
        type: item.type
      })

    $scope.cancel = $mdDialog.cancel
  })
})()
