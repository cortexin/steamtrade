angular.module('steam').controller('ItemBuyController', function ($scope, $routeParams) {
  $scope.stage = 'await'
  $scope.notifications.send('buy', {offer: $routeParams.offerId})

  /*
   * item received from the seller, proceed to payment
   */
  $scope.$watch('notifications.offer', (offer) => {
    if (!offer) { return }

    $scope.offer = offer
    console.log(offer)
    $scope.stage= 'pay'
  })

  /*
   * payment successful, send items
   */
  $scope.next = () => {
    $scope.stage = 'receive'
    $scope.notifications.send('transfer', {offer: $routeParams.offerId})
  }
})
