angular.module('steam').controller('ToastConfirmController', function ($mdToast, message) {
  let vm = this
  vm.message = message
  vm.confirm = $mdToast.hide
  vm.decline = $mdToast.cancel
})
.controller('ToastNotifyController', function (message) {
  let vm = this
  vm.message = message
})
.controller('ToastErrorController', function (message) {
  let vm = this
  vm.message = message
})

const TOAST_URL = '/static/templates/toasts/'

const ERROR_TOAST = (message) => ({
  templateUrl: '/static/templates/toasts/error.html',
  locals: { message },
  controllerAs: 'vm',
  controller: 'ToastErrorController'
})

const SUCCESS_TOAST = (message) => ({
  templateUrl: TOAST_URL + 'notify.html',
  locals: { message },
  controller: 'ToastNotifyController',
  controllerAs: 'vm'
})
