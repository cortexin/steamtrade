(function () {
  'use strict'

  const sortFilters = {
    '-price': 'Цене',
    '-reputation': 'Репутации продавца',
    '-amount': 'Количеству предметов'
  }

  angular.module('steam').controller('ItemDetailsController', function ($scope, $routeParams, $mdDialog, $location,
            Catalogue, Requests, CounterOffers) {

    $scope.sortFilters = sortFilters
    $scope.filters = {}
    function DIALOG (event, update = true) {
      return {
        controller: 'DemandOfferDialogController',
        templateUrl: $scope.TEMPLATE_ROOT + 'dialogs/demand.html',
        targetEvent: event,
        locals: {
          item: $scope.item,
          update: update
        },
        clickOutsideToClose: false
      }
    }
    function CO_DIALOG (event, offer, update = true) {
      return {
        controller: 'CounterOfferDialogController',
        controllerAs: 'vm',
        templateUrl: $scope.TEMPLATE_ROOT + 'dialogs/counterOffer.html',
        locals: {
          offer: offer,
          update: update
        },
        targetEvent: event
      }
    }

    Catalogue.offers($routeParams.itemName).then(
      ({data}) => { $scope.item = data },
      ({data}) => $scope.error_toast(data)
    )

    $scope.Offer = {
      demand: _demand,
      edit: _edit,
      remove: _remove,
      buy: _buy
    }

    function _demand (event) {
      $mdDialog.show(DIALOG(event)).then(function (request) {
        Requests.submit(request).then(
          () => { $scope.item.requested = true },
          ({data}) => $scope.error_toast(data)
        )
      })
    }

    function _edit (event) {
      $mdDialog.show(DIALOG(event, true)).then((request) => {
        Requests.update(request).then(
          (r) => { true },
          ({data}) => $scope.error_toast(data)
        )
      })
    }

    function _remove (event) {
      let confirm = $mdDialog
        .confirm()
        .title('Вы уверены?')
        .ok('Да')
        .cancel('Нет')

      $mdDialog.show(confirm).then(() => {
        Requests.remove($scope.item.classid).then(
          () => {
            $scope.item.requested = false
          },
          ({data}) => $scope.error_toast(data)
        )
      })
    }

    function _buy (offer) {
      $location.url('/item/' + $routeParams.itemName + '/' + offer.id)
    }

    $scope.cOffer = {
      create: _createCOffer,
      update: _updateCOffer
    }

    function _createCOffer (event, offer) {
      let prompt = $mdDialog.prompt(CO_DIALOG(event, offer, false))

      $mdDialog.show(prompt).then((price) => {
        CounterOffers.create(offer, price).then(
          ({data}) => { offer.counter_offers = [data] },
          ({data}) => $scope.error_toast(data)
        )
      })
    }

    function _updateCOffer (event, offer) {
      let prompt = $mdDialog.prompt(CO_DIALOG(event, offer))

      $mdDialog.show(prompt).then(
        (price) => {
          CounterOffers.update(offer.counter_offers[0], price).then(
            ({data}) => { offer.counter_offers = [r.data] },
            ({data}) => $scope.error_toast(data)
          )
        },
        (deleted) => {
          if (deleted)
            offer.counter_offers = []
        }
      )
    }
  })
})()
