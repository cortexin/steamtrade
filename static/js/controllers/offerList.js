(function () {
  'use strict'

  angular.module('steam').controller('OfferListController', function ($scope, $mdDialog, $mdToast, Offers) {
    Offers.list().then(
      ({data}) => {
        $scope.offers = data.offers
        // add notifications
        angular.forEach($scope.offers, function (offer) {
          if (data.notifications.indexOf(offer.id) !== -1)
            offer.hasCounterOffers = true
        })
      },
      ({data}) => $scope.error_toast(data)
    )

    $scope.removeOffer = (offer, i) => {
      let confirm = $mdDialog
        .confirm()
        .title('Вы уверены?')
        .textContent('Вы уверены что хотите снять с продажи товар "' + offer.item.name + '"?')
        .ok('Да')
        .cancel('Нет')

      $mdDialog.show(confirm).then(() => {
        Offers.cancel(offer).then(
          () => {
            $scope.offers.splice(i, 1)
            // $mdToast.show($mdToast.success())
          },
          ({data}) => $scope.error_toast(data)
        )
      })
    }

    $scope.showCounterOffers = (offer, event) => {
      $mdDialog.show({
        controller: 'CounterOfferListController',
        controllerAs: 'vm',
        templateUrl: $scope.TEMPLATE_ROOT + 'dialogs/counterOfferList.html',
        locals: {
          offer: offer
        },
        targetEvent: event
      })
    }
  })
})()
