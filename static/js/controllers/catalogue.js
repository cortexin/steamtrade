(function () {
  'use strict'

  angular.module('steam').controller('CatalogueController', function (Catalogue, $scope, $location, $http) {
    $scope.search = {}
    $scope.games = {
      'Team Fortress 2': '440',
      'Dota 2': '570',
      'Counter-Strike': '730'
    }
    $scope.items = []
    $scope.pagination = {
      current: 1,
      itemsPerPage: 25,
      totalItems: 10000,
      pageChanged: _fetch
    }
    $scope.tags = {}
    $scope.tags.filters = []

    $scope.details = (item) => $location.url('/item/' + item.pk)

    $scope.gameSelected = (appid) => {
      $scope.selectedAppId = appid
      $scope.pagination.current = 1
      $scope.tags.filters = []
      _fetch()

      if (!$scope.tags[appid]) {
        _fetchTags()
      }
    }

    $scope.resetFilters =  () => { $scope.tags.filters = [] }
    $scope.filter = _fetch

    $scope.search = () => {
      $location.search('search', $scope.search.input)
      $scope.search.input = ''
      _fetch()
    }

    function _fetch (page = $scope.pagination.current) {
      Catalogue.get($location.search(), $scope.selectedAppId, page, $scope.tags.filters)
        .then(
          ({data}) => {
            $scope.items = data.results
            $scope.pagination.totalItems = data.count
          },
          ({data}) => $scope.error_toast(data)
        )
    }

    function _fetchTags () {
      $http.get('/api/tags/' + $scope.selectedAppId).then(
        processTags,
        ({data}) => $scope.error_toast(data)
      )

      // TODO: move this to backend
      function processTags ({data}) {
        let categories = {}
        data.forEach((tag) => {
          const obj = {id: tag.id, name: tag.tag.name}
          const cat = tag.tag.category_name

          if (categories[cat]) {
            categories[cat].push(obj)
          } else {
            categories[cat] = [obj]
          }
        })
        $scope.tags[$scope.selectedAppId] = categories
      }
    }
  })
})()
