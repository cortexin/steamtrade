(function () {
  'use strict'

  angular.module('steam')
  .config(function ($mdIconProvider) {
    const SVG = '/media/svg/'

    $mdIconProvider
      .icon('ruble', SVG + 'ruble.svg')
      .icon('mailbox', SVG + 'mailbox.svg')
      .icon('remove', SVG + 'remove.svg')
      .icon('online', SVG + 'online.svg')
      .icon('offline', SVG + 'offline.svg')
      .icon('previous', SVG + 'previous.svg')
      .icon('next', SVG + 'next.svg')
      .icon('down', SVG + 'down.svg')
      .icon('up', SVG + 'up.svg')
      .icon('success', SVG + 'success.svg')
      .icon('error', SVG + 'error.svg')
  })
  .config(function ($mdThemingProvider) {
    $mdThemingProvider
      .theme('default')
      .primaryPalette('green')
      .accentPalette('teal')
      .backgroundPalette('grey')
      .dark()
  })
  .config(function ($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
  })
  .config(function ($locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    })
    $locationProvider.hashPrefix('!')
  })

})()
