(function () {
  'use strict'

  angular.module('steam').run(function ($rootScope, $mdToast, Notifications) {
    $rootScope.__imageUrl = 'http://cdn.steamcommunity.com/economy/image/'
    $rootScope.TEMPLATE_ROOT = '/static/templates/'

    $rootScope.notifications = Notifications

    $rootScope.error_toast = m => $mdToast.show(ERROR_TOAST(m))
    $rootScope.success_toast = m => $mdToast.show(SUCCESS_TOAST(m))
  })
})()
