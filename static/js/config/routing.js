(function () {
  'use strict'

  const TEMPLATE = '/static/templates/'

  angular.module('steam').config(function ($routeProvider) {
    $routeProvider
      .when('/index', {
        controller: 'MainController',
        templateUrl: TEMPLATE + 'main.html'
      })
      .when('/inventory', {
        controller: 'InventoryController',
        templateUrl: TEMPLATE + 'inventory.html'
      })
      .when('/browse', {
        controller: 'BrowseController',
        templateUrl: TEMPLATE + 'browse.html'
      })
      .when('/catalogue', {
        controller: 'CatalogueController',
        templateUrl: TEMPLATE + 'catalogue.html'
      })
      .when('/profile', {
        controller: 'ProfileController',
        templateUrl: TEMPLATE + 'profile.html'
      })
      .when('/profile/offers', {
        controller: 'OfferListController',
        templateUrl: TEMPLATE + 'offerList.html'
      })
      .when('/item/:itemName', {
        controller: 'ItemDetailsController',
        templateUrl: TEMPLATE + 'itemDetails.html'
      })
      .when('/item/:itemName/:offerId', {
        controller: 'ItemBuyController',
        templateUrl: TEMPLATE + 'buyItem.html'
      })
      .when('/mailbox', {
        controller: 'MailboxController',
        templateUrl: TEMPLATE + 'mailbox.html'
      })
      .otherwise('/index')
    })
})()
