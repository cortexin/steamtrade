from django.apps import AppConfig


class TradingConfig(AppConfig):
    name = 'trading'
    verbose_name = 'Торговля'

    def ready(self):
        from . import signals
