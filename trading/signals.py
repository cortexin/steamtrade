from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse

from .channels.notifications import notify_and_cache
from .cache import CounterOfferCache, UserOnlineCache
from .models import Offer, CounterOffer


def update_user_reputation(sender, instance, **kwargs):
    print('Saving Offer. Update fields:', kwargs['update_fields'])
    if 'buyer' in kwargs['update_fields']:
        instance.seller.reputation += 1
        instance.seller.save()


def update_item_popularity(sender, instance, **kwargs):
    if 'buyer' in kwargs['update_fields']:
        instance.item.popularity += 1
        instance.item.save()


@receiver(post_save, sender=CounterOffer)
def counter_offer_notification(sender, instance, **kwargs):
    if kwargs['created']:
        # CounterOfferCache.add(instance.offer.seller.pk, instance.pk)
        message = """Поступило контрпредложение на товар {}. Предлагаемая цена:
                    {}, Ваша изначальная цена: {}""".format(
                        instance.offer.item.market_name,
                        instance.price,
                        instance.offer.price
                    )
        print(message)
        notify_and_cache(instance.offer.seller,
                        message,
                        type="confirm",
                        image=instance.offer.item.icon_url,
                        confirm=reverse('counter-offer-accept',
                                        kwargs={'pk': instance.pk}),
                        decline=reverse('counter-offer-list')
                        )
