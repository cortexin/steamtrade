from django.conf.urls import url
from rest_framework.routers import SimpleRouter

from .views import (FetchUserItemsView,
                    OfferViewSet,
                    CounterOfferViewSet,
                    CounterOfferListView,
                    OfferRequestViewSet,
                    ItemListView,
                    ItemRetrieveView,
                    TagListView, )


router = SimpleRouter()
router.register(r'offers', OfferViewSet, base_name='offer')
router.register(r'requests', OfferRequestViewSet, base_name='offer-request')
router.register(r'counteroffers', CounterOfferViewSet,
                base_name='counter-offer')

urlpatterns = [
    url(r'fetch$', FetchUserItemsView.as_view(), name='fetch-items'),
    url(r'counteroffers/list/(?P<offer>\d+)', CounterOfferListView.as_view(),
        name='counter-offer-list'),
    url(r'items/(?P<pk>[\w\s]+)', ItemRetrieveView.as_view(), name='item-detail'),
    url(r'catalogue/(?P<appid>\d+)', ItemListView.as_view(), name='item-list'),
    url(r'tags/(?P<appid>\d+)', TagListView.as_view(), name='tag-list'),
]

urlpatterns += router.urls
