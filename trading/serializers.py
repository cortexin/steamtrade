from rest_framework import serializers
from rest_framework_hstore.fields import HStoreField

from .models import (CounterOffer, ItemClass, ItemTag, Offer, OfferRequest)
from .cache import UserOnlineCache

from authentication.serializers import PKRelatedCurrentUserDefaultField


class TagSerializer(serializers.ModelSerializer):
    tag = HStoreField()

    class Meta:
        model = ItemTag
        fields = ['id', 'tag']


class ItemListSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='market_name')
    price = serializers.FloatField()
    amount = serializers.IntegerField()

    class Meta:
        model = ItemClass
        fields = ['pk', 'icon_url', 'name', 'price', 'amount']


class NestedItemClassSeralizer(serializers.ModelSerializer):
    """
    A serializer for offer lists.
    """
    name = serializers.CharField(source='market_name')
    url = serializers.SerializerMethodField()

    class Meta:
        model = ItemClass
        fields = ['url', 'icon_url', 'name']

    def get_url(self, obj):
        """
        client-side route to the item class
        """
        return '/item/%s/' % obj.pk


class CounterOfferSerializer(serializers.ModelSerializer):
    client = PKRelatedCurrentUserDefaultField

    class Meta:
        model = CounterOffer
        fields = ['price', 'id', 'offer', 'client']
        extra_kwargs = {
            'offer': {'write_only': True}
        }


class BaseOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = ['id', 'amount', 'price']


class OfferSerializer(BaseOfferSerializer):
    """Item detail -> Item Offer List"""
    seller_online = serializers.SerializerMethodField()
    # should be related key
    counter_offers = CounterOfferSerializer(read_only=True, many=True)
    seller = PKRelatedCurrentUserDefaultField

    class Meta(BaseOfferSerializer.Meta):
        fields = BaseOfferSerializer.Meta.fields + ['instanceid', 'type',
                                                    'allow_counteroffers',
                                                    'seller_online',
                                                    'counter_offers',
                                                    'item',
                                                    'seller',
                                                    'send_offer']
        extra_kwargs = {'send_offer': {'write_only': True}}

    def get_seller_online(self, obj):
        return UserOnlineCache.check(obj.pk)


class OfferListSerializer(BaseOfferSerializer):
    """/MyOffers"""
    item = NestedItemClassSeralizer()
    purchased = serializers.SerializerMethodField()
    # TODO: counteroffers = counterofferserializer many=True
    has_counteroffers = serializers.SerializerMethodField(read_only=True)

    class Meta(BaseOfferSerializer.Meta):
        fields = BaseOfferSerializer.Meta.fields + ['creation_timestamp',
                                                    'purchased', 'item',
                                                    'has_counteroffers',
                                                    'code']

    def get_has_counteroffers(self, obj):
        return bool(obj.has_counteroffers)

    def get_purchased(self, obj):
        """
        Has the item already been purchased or not?
        """
        return bool(obj.purchase_timestamp)


class BaseOfferRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfferRequest
        fields = ['amount', 'price', 'item']


class OfferRequestSerializer(BaseOfferRequestSerializer):
    client = PKRelatedCurrentUserDefaultField
    name = serializers.CharField(read_only=True)
    icon_url = serializers.CharField(read_only=True)

    class Meta(BaseOfferRequestSerializer.Meta):
        fields = BaseOfferRequestSerializer.Meta.fields + ['client',
                                                           'request_timestamp',
                                                           'name',
                                                           'icon_url']


class ItemDetailSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)
    descriptions = serializers.ListField(child=HStoreField())

    name = serializers.CharField(source='market_name')
    offers = OfferSerializer(read_only=True, many=True)
    requested = serializers.BooleanField(read_only=True, required=False)

    class Meta:
        model = ItemClass
        fields = ['tags', 'descriptions', 'icon_url',
                  'icon_url_large', 'name', 'name_color',
                  'background_color', 'item_type',
                  'appid', 'classid', 'offers', 'requested']
