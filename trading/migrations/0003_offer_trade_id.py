# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-06-10 20:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trading', '0002_auto_20170604_1333'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='trade_id',
            field=models.CharField(max_length=16, null=True),
        ),
    ]
