from rest_framework import permissions


class IsOfferOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.cart == request.user.cart


class HasTradeURLEnabled(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.trade_offer_url)


class AllowCreateOnce(permissions.BasePermission):
    """
    Allow client to create an object if and only if there are no other objects
    of this type created by the client.
    NB: only for generic views
    """
    def has_permission(self, request, view):
        return True
