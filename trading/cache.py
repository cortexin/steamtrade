import json
from django.conf import settings

r = settings.REDIS_CONN(True)


class Cache:
    def __init__(self, prefix: str):
        self.prefix = prefix + ':'

    def add(self, key, value):
        return self.command('sadd', key, value)

    def get(self, key):
        return self.command('smembers', key)

    def clear(self, key):
        return self.command('delete', key)

    def remove(self, key):
        return self.command('srem', key)

    def command(self, attr, key, *args):
        return getattr(r, attr)(self.prefix + str(key), *args)


class KeylessCache(Cache):
    def add(self, key):
        return self.command('sadd', key)

    def check(self, value):
        return self.command('sismember', value)

    def command(self, attr, *args):
        return getattr(r, attr)(self.prefix, *args)


CounterOfferCache = Cache('counteroffer')
MailBoxCache = Cache('mailbox')
UserOnlineCache = KeylessCache('online')
