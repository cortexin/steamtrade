
from django.conf import settings
from collections import OrderedDict

from .models import ItemClass, ItemTag

from steampy.client import SteamClient

api = SteamClient(settings.SOCIAL_AUTH_STEAM_API_KEY)


def get_schema_items(game: str):
    items = api.call('IEconItems_%s.GetSchema' % game)['result']['items']
    return {item['defindex']: item for item in items}


def get_classid(game, items):
    link = api.api_call("ISteamEconomy.GetAssetPrices", appid=game)['result']['assets']

    for ln in link:
        defindex = ln['class'][0]['value']
        print(defindex)
        if not items.get(defindex, None):
            continue
        print('got here')
        items[defindex]['classid'] = ln['classid']

    return items


def item_descriptions(game):
    params = dict(appid=game, key=settings.SOCIAL_AUTH_STEAM_API_KEY)
    items = api.api_call("GET", "ISteamEconomy", "GetAssetPrices", 'v1',
                         params).json()['result']['assets']
    print('got here')
    classids = OrderedDict()
    descriptions = {}
    for i, item_data in enumerate(items):
        classids['classid' + str(i % 100)] = item_data['classid']
        if i % 100 == 99 or i+1 == len(items):
            params.update(dict(class_count=i % 100,
                          language='ru'))
            params.update(classids)
            descriptions.update(api.api_call('GET', 'ISteamEconomy', 'GetAssetClassInfo',
                                'v1', params).json()['result'])
    return descriptions


def persist_descriptions(game, descriptions):
    for class_id, d in descriptions.items():

        try:
            item = ItemClass.objects.create(classid=class_id,
                                            appid=game,
                                            icon_url=d['icon_url'],
                                            icon_url_large=d['icon_url_large'],
                                            name=d['name'],
                                            market_name=d['name'],
                                            market_hash_name=d['market_hash_name'],
                                            name_color=d['name_color'],
                                            background_color=d['background_color'],
                                            item_type=d['type'],
                                            descriptions=list(d['descriptions'].values()))
            for tag in d['tags'].values():
                item_tag =  ItemTag.objects.filter(tag=tag, appid=game)

                if item_tag.exists():
                    item_tag = item_tag.first()
                    print('Tag exists')
                else:
                    item_tag = ItemTag.objects.create(tag=tag, appid=game)
                item.tags.add(item_tag)
                item.save()
        except:
            continue
        print('New item %s | %s' % (class_id, game))


def migrate_schema(game: str):
    descriptions = item_descriptions(game)
    persist_descriptions(game, descriptions)
