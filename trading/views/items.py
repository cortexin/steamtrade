from django.db.models import Min, Count, Prefetch

from rest_framework import filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.exceptions import ParseError

from ..pagination import SimplifiedPageNumberPagination
from ..serializers import ItemListSerializer, ItemDetailSerializer
from ..models import ItemClass, CounterOffer, Offer

from rest_framework.generics import ListAPIView, RetrieveAPIView, get_object_or_404
import urllib


class ItemListView(ListAPIView):
    pagination_class = SimplifiedPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['market_name', ]
    serializer_class = ItemListSerializer

    def post(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        appid = self.kwargs.get('appid')
        qs = ItemClass.objects.filter(appid=appid)
        qs = qs.annotate(price=Min('offers__price'), amount=Count('offers'))

        tags = self.request.data.get('filters')
        if isinstance(tags, list):
            for tag in tags:  # django orm cannot into m2m contains -__-
                qs = qs.filter(tags=tag)
        return qs


class ItemRetrieveView(RetrieveAPIView):
    serializer_class = ItemDetailSerializer


    def get_queryset(self):
        prefetch = Prefetch(
            'counter_offers',
            queryset=CounterOffer.objects.only('id', 'price').filter(
                client=self.request.user
                )
            )
        offer_qs = Offer.objects.prefetch_related(prefetch)
        qs = ItemClass.objects.prefetch_related(
            Prefetch('offers', queryset=offer_qs))
        qs = qs.annotate(requested=Count('offer_requests'))
        return qs
