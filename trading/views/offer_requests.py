from django.db.models import F

from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from ..models import OfferRequest
from ..serializers import OfferRequestSerializer, BaseOfferRequestSerializer
from ..permissions import HasTradeURLEnabled, AllowCreateOnce


class OfferRequestViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated, HasTradeURLEnabled, AllowCreateOnce]
    lookup_field = 'item__pk'

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        kwargs = {
            self.lookup_field: self.kwargs[self.lookup_field],
            'client': self.request.user
        }
        return get_object_or_404(queryset, **kwargs)

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return BaseOfferRequestSerializer
        return OfferRequestSerializer

    def get_queryset(self):
        qs = OfferRequest.objects.filter(client=self.request.user)
        qs = qs.annotate(icon_url=F('item__icon_url'), name=F('item__name'))
        return qs
