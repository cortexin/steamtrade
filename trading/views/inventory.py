from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from ..utils import get_data_or_parse_error
from ..inventory import Inventory


class FetchUserItemsView(APIView):
    """
    Fetch user's inventory from Steam API
    """
    permission_classes = [IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        app_id = get_data_or_parse_error(request, 'app_id')

        items = Inventory.get_processed(request.user, app_id)

        return Response({'items': items},
                        status=status.HTTP_200_OK)
