from django.db.models import Count

from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from bots.models import Manager
from bots.tasks import validate_preoffer

from ..cache import CounterOfferCache
from ..inventory import Inventory
from ..models import Offer
from ..permissions import HasTradeURLEnabled
from ..serializers import OfferSerializer, OfferListSerializer
from ..utils import get_data_or_parse_error


class OfferViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated, HasTradeURLEnabled, ]

    def create(self, request, *args, **kwargs):
        Inventory.assert_can_offer(request.user, request.data)
        send_offer = get_data_or_parse_error(request, 'send_offer')

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        response = {}

        if send_offer:
            bot = Manager.get_free()
            serializer.save(tradebot=bot.username)
            response['url'] = bot.url
            validate_preoffer(request.user, serializer.data['id'])
        else:
            serializer.save()

        response['offer'] = serializer.data

        headers = self.get_success_headers(serializer.data)
        return Response(response, status=status.HTTP_201_CREATED, headers=headers)

        return super().create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        # get counteroffer notifications for the current user
        notifications = CounterOfferCache.get(request.user.pk)
        # clear the counteroffer cache
        CounterOfferCache.clear(request.user.pk)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'offers': serializer.data,
                         'notifications': notifications})

    def get_queryset(self):
        qs = Offer.objects.all()
        if self.action not in ['buy', ]:
            qs = qs.filter(seller=self.request.user)
        if self.action == 'list':
            # TODO: change to:
            # qs = qs.prefetch_related('counter_offers')

            qs = qs.annotate(has_counteroffers=Count('counter_offers'))
        return qs

    @detail_route(methods=['post'])
    def buy(self, request, *args, **kwargs):
        offer = self.get_object()
        seller = offer.seller

        if seller.online:
            Channel('buy/add_to_pool').send({'pk': offer.pk})
        else:  # leave a notification?
            True

        return Response()

    def get_serializer_class(self):
        if self.action == 'create':
            return OfferSerializer
        elif self.action == 'list':
            return OfferListSerializer
