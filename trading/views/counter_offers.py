from django.db.models import Prefetch

from rest_framework import mixins
from rest_framework.decorators import detail_route
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from ..cache import UserOnlineCache
from ..channels.notifications import notify_and_cache
from ..exceptions import UserNotOnline
from ..models import CounterOffer, ItemClass
from ..permissions import HasTradeURLEnabled
from ..serializers import CounterOfferSerializer


class CounterOfferListView(ListAPIView):
    serializer_class = CounterOfferSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        offer_pk = self.kwargs.get('offer')
        qs = CounterOffer.objects.filter(offer__seller=self.request.user)
        return qs.filter(offer__pk=offer_pk)


class CounterOfferViewSet(mixins.CreateModelMixin,
                          mixins.UpdateModelMixin,
                          mixins.DestroyModelMixin,
                          GenericViewSet):
    permission_classes = [IsAuthenticated, HasTradeURLEnabled]
    serializer_class = CounterOfferSerializer

    def get_queryset(self):
        qs = CounterOffer.objects.all()
        if self.action == 'list':
            qs = qs.filter(offer__seller=self.request.user)
        elif self.action in ['accept', 'create']:
            prefetch_qs = ItemClass.objects.only('market_name', 'icon_url')
            qs = qs.prefetch_related(Prefetch('offer__item',
                                              queryset=prefetch_qs))
        else:
            qs = qs.filter(client=self.request.user)

        return qs

    @detail_route(methods=['post'])
    def accept(self, request, *args, **kwargs):

        counter_offer = self.get_object()

        message = 'Продавец принял ваше контрпредложение по предмету %s' %\
            counter_offer.offer.item.market_name
        notify_and_cache(counter_offer.client,
                        message,
                        type='notify',
                        image=counter_offer.offer.item.icon_url)
        return Response()
