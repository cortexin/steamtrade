from rest_framework.generics import ListAPIView

from trading.models import ItemTag
from trading.serializers import TagSerializer


class TagListView(ListAPIView):
    serializer_class = TagSerializer

    def get_queryset(self):
        return ItemTag.objects.filter(appid=self.kwargs.get('appid'))
