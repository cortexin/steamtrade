from rest_framework.exceptions import ParseError


def get_data_or_parse_error(request, key):
    try:
        return request.data[key]
    except KeyError:
        raise ParseError(detail="%s: this field is required." % key)
