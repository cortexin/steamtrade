import json

from django.contrib.auth.models import User
from django.conf import settings

from steampy.client import SteamClient, Asset
from steampy.utils import GameOptions

from channels import Group
from channels.auth import channel_session_user_from_http
from channels.generic.websockets import WebsocketConsumer

from authentication.utils import check_discount

from bots.tasks import validate_postoffer
from .notifications import notify, flush, offer_ready
from ..cache import UserOnlineCache, MailBoxCache
from ..models import Offer


class NotificationConsumer(WebsocketConsumer):
    http_user = True
    strict_ordering = False

    def connection_groups(self, **kwargs):
        return ['notify-%s' % self.message.user.pk]

    def connect(self, message, **kwargs):
        self.message.reply_channel.send({"accept": True})
        user = self.message.user

        if user.is_authenticated:

            # add user to the list of online users
            UserOnlineCache.add(user.pk)

            # send user settings
            settings = {
                'allow_sound': user.allow_sound,
                'allow_toasts': user.allow_toasts,
                'url_enabled': bool(user.trade_offer_url),
                'discount': check_discount(user)
            }
            self.send(type='settings', payload=settings)

            notifications = MailBoxCache.get(user.pk)
            if notifications:
                notify(user, None, 'update', new_notifications=notifications)

    def send(self, type, payload):
        message = json.dumps({
            'type': type,
            'payload': payload
        })
        return super().send(message)

    def receive(self, text=None, **kwargs):
        message = json.loads(text)
        action = message.get('action')
        action = getattr(self, action, None)
        if action:
            action(message['content'], **kwargs)

    def flush(self, message, **kwargs):
        print(self.message)
        print('Flushing...')
        flush(self.message.user)

    def buy(self, message, **kwargs):
        offer =  Offer.objects.get(pk=message['offer'])

        if offer.send_offer:
            # bot has offer pending, proceed to payment
            offer_ready(self.message.user, offer)
        else:
            offer.buyer = self.message.user
            offer.save()

            # request item from the seller and launch validation cycle
            offer.request_item()
            validate_postoffer(offer.pk)


    def transfer(self, message, **kwargs):
        offer = Offer.objects.get(pk=message['offer'])
        offer.buyer = self.message.user
        offer.save()
        offer.accept_trade()
        offer.send_to_customer()
        print("Transferring %s" % offer)

        self.send('transfered', {
            'code': offer.code
        })

    def disconnect(self, message, **kwargs):
        UserOnlineCache.remove(self.message.user.pk)
        super().disconnect(message, **kwargs)
