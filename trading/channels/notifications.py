import json
from channels import Group

from authentication.utils import check_discount
from notifications.models import Notification
from trading.cache import UserOnlineCache, MailBoxCache


def notify_and_cache(user, message, type, **kwargs):
    """ send a WS notification to the user if they are online. Otherwise, cache
    it and send as soon as the user appears online.
    """
    if UserOnlineCache.check(user.pk):
        notify(user, message, type, **kwargs)

    MailBoxCache.add(user.pk, dict(message=message, type=type, **kwargs))


def notify(user, payload, type, **kwargs):
    _notify(user, json.dumps(dict(
        payload=payload, type=type, **kwargs)))


def _notify(user, text):
    Group('notify-%s' % user.pk).send({'text': text})


def flush(user):
    notifications = [Notification(data=eval(n), user=user)
                     for n in MailBoxCache.get(user.pk)]
    if notifications:
        Notification.objects.bulk_create(notifications)
        MailBoxCache.clear(user.pk)
        print("Flushed.")

def offer_ready(user, offer):
    discount = check_discount(user)
    payload = {
        'discount': discount,
        'price': offer.price,
        'code': offer.code
    }
    notify(user, payload, 'offer')
