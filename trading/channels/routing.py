from channels.routing import route
from channels import route_class
from . import consumers


routes = [
    route_class(consumers.NotificationConsumer, path=r'/connect')
]
