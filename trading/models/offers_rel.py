from django.db import models

from authentication.models import User

from .item import ItemClass
from .offer import Offer


class CounterOffer(models.Model):
    client = models.ForeignKey(User, related_name='counter_offers')
    offer = models.ForeignKey(Offer, related_name='counter_offers')

    price = models.FloatField()

    class Meta:
        unique_together = ('client', 'offer')

    def __str__(self):
        return '%s ^ %s :: %s' % (self.client, self.price, self.offer)


class OfferRequest(models.Model):
    item = models.ForeignKey(ItemClass, related_name='offer_requests')
    client = models.ForeignKey(User, related_name='offer_requests')

    amount = models.PositiveSmallIntegerField(default=1)
    price = models.FloatField()

    request_timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        # there shouldn't be more than one offer request per item type per user
        unique_together = ('item', 'client')
        ordering = ('-price', )

    def __str__(self):
        return '%s :: %s' % (self.client, self.item)
