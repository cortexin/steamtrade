from django.db import models
from django.contrib.postgres.fields import HStoreField, ArrayField, JSONField


class ItemClass(models.Model):
    classid = models.CharField(max_length=12)
    appid = models.CharField(max_length=3)
    icon_url = models.CharField(max_length=256)
    icon_url_large = models.CharField(max_length=256)

    name = models.CharField(max_length=256)
    market_name = models.CharField(max_length=256)
    market_hash_name = models.CharField('Наименование в Steam',
                                        max_length=256, primary_key=True)

    name_color = models.CharField(max_length=6)
    background_color = models.CharField(max_length=6)
    item_type = models.CharField(max_length=256)

    descriptions = ArrayField(HStoreField())
    tags = models.ManyToManyField('ItemTag', related_name='items')

    allowed = models.BooleanField('Разрешен к торговле', default=True)

    popularity = models.PositiveSmallIntegerField('Популярность', default=0)

    max_price = models.FloatField('Максимальная цена', default=100)

    class Meta:
        verbose_name = 'Предмет'
        verbose_name_plural = 'Предметы'

    def __str__(self):
        return self.pk

class ItemTag(models.Model):
    tag = HStoreField()
    appid = models.CharField(max_length=3, null=True)

    def __str__(self):
        return self.tag.name
