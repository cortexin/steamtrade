from django.db import models
from authentication.models import User

from .item import ItemClass
from bots.models import TradeBot, Manager

from random import getrandbits


def get_code():
    return getrandbits(12)


class OfferManager(models.Manager):
    def available(self):
        return self.filter(buyer__is_null=True)

    def purchased(seld):
        return self.filter(buyer__is_null=False)


class Offer(models.Model):
    """
    Actual instance of an itme being traded by someone
    """
    item = models.ForeignKey(ItemClass, related_name='offers')

    # instance id might change after trade
    instanceid = models.CharField(max_length=12)

    # might depend on instanceid
    type = models.CharField(max_length=128)

    tradebot = models.CharField(max_length=20, null=True)
    # null --> trade offer type 2
    # !null --> trade offer type 1
    send_offer = models.BooleanField(default=False)

    seller = models.ForeignKey(User, related_name='offers')
    buyer = models.ForeignKey(User, null=True, blank=True,
                              related_name='purchases')

    allow_counteroffers = models.BooleanField(default=False)
    amount = models.PositiveSmallIntegerField()
    price = models.FloatField()

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    purchase_timestamp = models.DateTimeField(null=True, blank=True)

    trade_id = models.CharField(max_length=16, null=True)

    code = models.PositiveSmallIntegerField(default=get_code)

    objects = OfferManager()

    def accept_trade(self):
        self.bot.accept(self)

    def send_to_customer(self):
        self.bot.transfer(self)

    def request_item(self):
        self.bot.request(self)

    @property
    def bot(self):
        if not hasattr(self, '_bot'):
            if not self.tradebot:  # bot not assigned yet
                # TODO:  change return value to username
                self.tradebot = Manager.get_free().username
                self.save()

            bot = Manager.get_by_name(self.tradebot)
            setattr(self, '_bot', bot)
        return self._bot

    def __str__(self):
        return '%s --> %s | %s' % (self.seller, self.buyer, self.item)
