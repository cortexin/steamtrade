import requests
from collections import defaultdict

from django.db.models import Sum
from rest_framework.exceptions import ParseError

from .exceptions import (SteamAPIError, ProfilePrivateError, NoGameError,
                         WrongItemClassError, WrongAmountError, NoItemsError)
from .models import Offer, ItemClass


class Inventory:
    def __init__(self, user, appid):
        self.user = user
        self.user_uid = user.social_auth.first().uid
        self.appid = appid

        inventory = self.fetch_user_inventory()
        self.assets = inventory['assets']
        self.descriptions = inventory['descriptions']

    def fetch_user_inventory(self):
        # userid = "76561197999678649"
        url = "http://steamcommunity.com/inventory/{userid}/{appid}/2".format(
            userid=self.user_uid, appid=self.appid)

        headers = {"Accept-Language": "ru-RU,ru;q=0.5"}

        r = requests.get(url, headers=headers)

        if r.content == b'null':
            raise ProfilePrivateError

        try:
            r = r.json()
        except:
            raise SteamAPIError

        error = r.get('error')

        if error == 'Called method busy, action not taken (10)':  # this should not happen
            raise NoGameError
        elif error == 'Failure (2)':
            raise NoGameError

        if r.get('total_inventory_count') == 0:
            raise NoItemsError

        return r

    @classmethod
    def get_processed(cls, user, appid):
        """ Get processed and filtered descriptions of the items in the user app
        inventory.
        """
        inventory = cls(user, appid)
        inventory.filter()
        inventory.add_amounts()
        return inventory.descriptions

    @classmethod
    def assert_can_offer(cls, user, offer):
        """Assert that a given amount of a given item may be traded by the user
        """
        appid = offer['appid']
        inventory = cls(user, appid)
        inventory.filter()
        try:
            assert offer.get('classid', None) in cls.allowed_items()
        except AssertionError:
            raise WrongItemClassError

        inventory_amounts = inventory.get_inventory_amounts()
        offer_amounts = inventory.get_offer_amounts(
            instanceid=offer['instanceid'])

        available_amount = inventory_amounts[offer['classid']] - \
            offer_amounts.get(offer['classid'], 0)

        try:
            assert int(offer['amount']) <= available_amount
        except AssertionError:
            raise WrongAmountError

        # TODO: check for item price

    def filter(self):
        """Leave only those items which may be traded on our website.
        """
        allowed_items = self.allowed_items()
        # self.assets = [asset for asset in self.assets
        #                if asset['classid'] in allowed_items]
        self.descriptions = [desc for desc in self.descriptions
                             if desc['classid'] in allowed_items
                             and desc['tradable'] == 1]

    @classmethod
    def allowed_items(cls):
        """List classids of items allowed for trade. TODO: change to instanceids
        """
        qs = ItemClass.objects.values('classid').all()
        return [item_class['classid'] for item_class in qs]

    def add_amounts(self):
        """ Add offer and inventory item amounts to item descriptions.
        """
        inventory_amounts = self.get_inventory_amounts()
        offer_amounts = self.get_offer_amounts()

        for desc in self.descriptions:
            desc['amount'] = inventory_amounts[desc['classid']]
            desc['amount_offered'] = offer_amounts.get(desc['classid'], 0)

    def get_inventory_amounts(self):
        amounts = defaultdict(int)
        for asset in self.assets:
            amounts[asset['classid']] += int(asset['amount'])
        return amounts

    def get_offer_amounts(self, **kwargs):
        amounts = Offer.objects.filter(seller=self.user,
                                       buyer__isnull=True,
                                       item__appid=self.appid,
                                       **kwargs)

        amounts = amounts.values('item').annotate(amount=Sum('amount'))
        return {v['item']: v['amount'] for v in amounts}
