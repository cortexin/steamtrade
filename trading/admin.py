from django.contrib import admin

from trading.models import ItemClass


class ItemAdmin(admin.ModelAdmin):
    list_display = ['market_hash_name', 'popularity', 'allowed', 'max_price']
    search_fields = ['market_hash_name']
    actions = ['allow_trading', 'disallow_trading']

    def allow_trading(self, request, queryset):
        queryset.update(allowed=True)

    allow_trading.short_description = "Разрешить выбранные предметы к торговле"

    def disallow_trading(self, request, queryset):
        queryset.update(allowed=False)

    disallow_trading.short_description = "Запретить торговлю выбранными предметами"


admin.site.register(ItemClass, ItemAdmin)
