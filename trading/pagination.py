from rest_framework import pagination
from rest_framework.response import Response


class SimplifiedPageNumberPagination(pagination.PageNumberPagination):
    """
    Since we use AngularJS for the front-end, we do not need some parts of the
    default PageNumberPagination class.
    """
    page_size = 25
    page_size_query_param = 'size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response(dict(
            count=self.page.paginator.count,
            results=data
        ))
