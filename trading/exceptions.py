from rest_framework.exceptions import APIException


class SteamAPIError(APIException):
    status_code = 502
    default_detail = "Steam API did not return the expected data. Please try again later."


class NoItemsError(APIException):
    status_code = 400
    default_detail = "You do not have any tradable items for this game."


class NoGameError(APIException):
    status_code = 417
    default_detail = "You do not have a copy of this game."


class ProfilePrivateError(APIException):
    status_code = 403
    default_detail = """Your game inventory is private. Please go to your settings
                    and mark it as 'public' in order to proceed.
                    """


class InvalidItemClassError(APIException):
    status_code = 400
    default_detail = "Invalid item class."


class UserNotOnline(APIException):
    status_code = 417
    default_detail = "This user is currently offline."


class WrongItemClassError(APIException):
    status_code = 400
    default_detail = "Wrong item class."


class WrongAmountError(APIException):
    status_code = 400
    default_detail = "Wrong amount of assets."
