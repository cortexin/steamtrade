from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.cache import cache


class User(AbstractUser):
    trade_offer_url = models.URLField(null=True)
    reputation = models.FloatField(default=0)

    allow_sound = models.BooleanField(default=True)
    allow_toasts = models.BooleanField(default=True)

    @property
    def online(self):
        return cache.get('%s:online' % self.pk)
