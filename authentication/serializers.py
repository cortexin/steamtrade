from rest_framework import serializers
from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['trade_offer_url', 'username', 'allow_sound', 'allow_toasts']


class NestedUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username', 'id']


PKRelatedCurrentUserDefaultField = serializers.PrimaryKeyRelatedField(
    queryset=User.objects.all(),
    default=serializers.CurrentUserDefault(),
    write_only=True
)
