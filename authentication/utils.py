from django.contrib.sessions.models import Session
from django.utils import timezone
from django.conf import settings

import xmltodict
import requests

def get_users_online():
    alive_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_ids = []
    for session in alive_sessions:
        data = session.get_decoded()
        user_ids.append(int(data.get('_auth_user_id', None)))

    print(user_ids)
    return user_ids


def check_discount(user):
    return 1,
    # discount = 0
    # # does not work. need some other user id
    # r = requests.get("http://steamcommunity.com/id/%s/?xml=1" % user)
    # r = xmltodict.parse(r.content)
    # print(r)
    #
    # r = r['profile']
    # if check_group(r):
    #     discount += 2
    # if check_username(r):
    #     discount += 2


    return discount

def check_group(profile):
    groups = profile['groups']['group']
    for group in groups:
        print(group)
        if group['@isPrimary'] == '1' and group['groupName'] == settings.STEAM_GROUP:
            return True

def check_username(profile):
    print(profile['steamID'])
    if settings.PROMO_NAME in profile['steamID']:
        return True
