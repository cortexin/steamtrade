from rest_framework.generics import RetrieveUpdateAPIView

from .models import User
from .serializers import UserSerializer


class UserView(RetrieveUpdateAPIView):
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user
