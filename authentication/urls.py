from django.conf.urls import url
from .views import UserView


urlpatterns = [
    url(r'profile/$', UserView.as_view(), name='user-detail'),
]
