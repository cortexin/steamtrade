from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from .models import Notification
from .pagination import NotificationPagination


class NotificationListView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        qs = Notification.objects.filter(user=self.request.user).values()
        qs = self.paginator.paginate_queryset(qs, self.request, view=self)
        qs = [notification['data'] for notification in qs]

        return self.paginator.get_paginated_response(qs)

    @property
    def paginator(self):
        if not hasattr(self, '_paginator'):
            self._paginator = NotificationPagination()
        return self._paginator
