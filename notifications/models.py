# create a new app for this maybe?
from django.db import models
from django.contrib.postgres.fields import JSONField

from authentication.models import User


class Notification(models.Model):
    data = JSONField()
    user = models.ForeignKey(User, related_name='notifications')

    class Meta:
        ordering = ('-id', )
